import * as util from '../util.js';

var vertextShaderText =
    `
precision mediump float;

attribute vec3 vertPosition;
attribute vec3 vertColor;

uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProjection;

varying vec3 fragColor;

void main()
{
    fragColor = vertColor;
    gl_Position =  mProjection * mView * mWorld * vec4(vertPosition, 1.0);
}
`;

var fragmentShaderText =
    `
precision mediump float;

varying vec3 fragColor;

void main()
{
    gl_FragColor = vec4(fragColor, 1.0);
}
`;

let cube = {
    vertices: [
        // Top
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,

        // Left
        -1.0, 1.0, 1.0,
        -1.0, -1.0, 1.0,
        -1.0, -1.0, -1.0,
        -1.0, 1.0, -1.0,

        // Right
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
        1.0, 1.0, -1.0,

        // Front
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        -1.0, -1.0, 1.0,
        -1.0, 1.0, 1.0,

        // Back
        1.0, 1.0, -1.0,
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, 1.0, -1.0,

        // Bottom
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
    ],
    colors: [
        // Top
        0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,

        // Left
        0.75, 0.25, 0.5,
        0.75, 0.25, 0.5,
        0.75, 0.25, 0.5,
        0.75, 0.25, 0.5,

        // Right
        0.25, 0.25, 0.75,
        0.25, 0.25, 0.75,
        0.25, 0.25, 0.75,
        0.25, 0.25, 0.75,

        // Front
        1.0, 0.0, 0.15,
        1.0, 0.0, 0.15,
        1.0, 0.0, 0.15,
        1.0, 0.0, 0.15,

        // Back
        0.0, 1.0, 0.15,
        0.0, 1.0, 0.15,
        0.0, 1.0, 0.15,
        0.0, 1.0, 0.15,

        // Bottom
        0.5, 0.5, 1.0,
        0.5, 0.5, 1.0,
        0.5, 0.5, 1.0,
        0.5, 0.5, 1.0,
    ],
    indices: [
        // Top
        0, 1, 2,
        0, 2, 3,

        // Left
        5, 4, 6,
        6, 4, 7,

        // Right
        8, 9, 10,
        8, 10, 11,

        // Front
        13, 12, 14,
        15, 14, 12,

        // Back
        16, 17, 18,
        16, 18, 19,

        // Bottom
        21, 20, 22,
        22, 20, 23
    ]
}

export function init() {
    console.log("Running...");

    var canvas = document.getElementById("game");
    var gl = canvas.getContext('webgl');

    if (!gl) {
        console.log("Webgl not supported, falling back to experimental-version");
        gl = canvas.getContext('experimental-webgl');
    }

    if (!gl) {
        alert("Webgl not supported by your browser");
    }

    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.frontFace(gl.CCW)
    gl.cullFace(gl.BACK);

    // Compile Shaders
    let vertexShader = util.compileShader(gl, vertextShaderText, gl.VERTEX_SHADER);
    let fragmentShader = util.compileShader(gl, fragmentShaderText, gl.FRAGMENT_SHADER);

    // Create Program
    let program = util.createProgram(gl, vertexShader, fragmentShader);
    let buffer = util.createBuffer(gl, cube);

    // Colors
    var boxColorBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, boxColorBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube.colors), gl.STATIC_DRAW);

    var colorAttribLocation = gl.getAttribLocation(program, 'vertColor');

    // Color
    gl.vertexAttribPointer(
        colorAttribLocation, // Attribute Location
        3, // Number of elements per attribute 
        gl.FLOAT, // Type of elements
        gl.FALSE, // Data is normilized
        3 * Float32Array.BYTES_PER_ELEMENT, // Size of individual vertex
        0 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
    );

    gl.enableVertexAttribArray(colorAttribLocation);
    gl.useProgram(program);

    let camera = util.cameraSettings(gl, program, canvas, [0, 35, -65]);

    let angle;
    let rotation = 90;
    let cubes = 250;
    let rotationsPerSecond = 2 // Speed
    let coolMode = false;

    let loop = () => {
        gl.clearColor(0.75, 0.85, 0.8, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        angle = performance.now() / 1000 / rotationsPerSecond * 2 * Math.PI;

        // Camera
        glMatrix.mat4.identity(camera.matrix.view);
        glMatrix.mat4.lookAt(camera.matrix.view, [0, 10, -10 - cubes - Math.sin(angle / 5) * cubes], [0, 0, 0], [0, 1, 0]);
        glMatrix.mat4.translate(camera.matrix.view, camera.matrix.view, [0, -cubes + Math.sin(angle / 5) * cubes, 0]);
        glMatrix.mat4.rotate(camera.matrix.view, camera.matrix.view, angle / 4, [0, 1, 0]);
        gl.uniformMatrix4fv(camera.location.view, gl.FALSE, camera.matrix.view);

        // Cubes
        for (let i = 1; i <= cubes; i++) {
            glMatrix.mat4.identity(camera.matrix.world);
            glMatrix.mat4.scale(camera.matrix.world, camera.matrix.world, [cubes / i, 1, cubes / i]);
            glMatrix.mat4.rotate(camera.matrix.world, camera.matrix.world, rotation * i, [0, 1, 0]);
            glMatrix.mat4.translate(camera.matrix.world, camera.matrix.world, [0, i * 2, 0]);
            gl.uniformMatrix4fv(camera.location.world, gl.FALSE, camera.matrix.world);

            util.drawBuffer(gl, program, buffer, cube, { num: 3, size: 3, offset: 0 }, gl.TRIANGLES);
        }

        if (coolMode) {
            for (let i = 1; i <= cubes; i++) {
                glMatrix.mat4.identity(camera.matrix.world);
                glMatrix.mat4.scale(camera.matrix.world, camera.matrix.world, [cubes / i, 1, cubes / i]);
                glMatrix.mat4.rotate(camera.matrix.world, camera.matrix.world, (angle * i) / 50, [0, 1, 0]);
                // glMatrix.mat4.rotate(camera.matrix.world, camera.matrix.world, rotation * i, [0, 1, 0]);
                glMatrix.mat4.translate(camera.matrix.world, camera.matrix.world, [0, i * 2, -10]);
                gl.uniformMatrix4fv(camera.location.world, gl.FALSE, camera.matrix.world);

                util.drawBuffer(gl, program, buffer, cube, { num: 3, size: 3, offset: 0 }, gl.TRIANGLES);
            }

            for (let i = 1; i <= cubes; i++) {
                glMatrix.mat4.identity(camera.matrix.world);
                glMatrix.mat4.scale(camera.matrix.world, camera.matrix.world, [cubes / i, 1, cubes / i]);
                glMatrix.mat4.rotate(camera.matrix.world, camera.matrix.world, rotation * i, [0, 1, 0]);
                glMatrix.mat4.translate(camera.matrix.world, camera.matrix.world, [0, i * 2, -10]);
                gl.uniformMatrix4fv(camera.location.world, gl.FALSE, camera.matrix.world);

                util.drawBuffer(gl, program, buffer, cube, { num: 3, size: 3, offset: 0 }, gl.TRIANGLES);
            }
        }

        requestAnimationFrame(loop);
    }

    requestAnimationFrame(loop);
}

window.init = init;