var vertextShaderText = 
`
precision mediump float;

attribute vec2 vertPosition;
attribute vec3 vertColor;

varying vec3 fragColor;

void main()
{
    fragColor = vertColor;
    gl_Position = vec4(vertPosition, 0.0, 1.0);
}
`;

var fragmentShaderText = 
`
precision mediump float;

varying vec3 fragColor;

void main()
{
    gl_FragColor = vec4(fragColor, 1.0);
}
`;

function genTriangleFan() {

    let k = 45;
    let r = .5;
    let cords = [];

    cords.push(0);
    cords.push(0);

    for(let i = 0; i < 2*Math.PI+0.001; i+=(2*Math.PI/k) ){

        let tmp = r * Math.cos(i);
        let tmp2 = r * Math.sin(i);

        if(i <= Math.PI) {
            tmp2 = r* Math.sin(i) * 1.9;
        }

        cords.push(tmp);
        cords.push(tmp2);

    }

    return cords;

}

var InitDemo = function() {
    console.log("Hello");

    var canvas = document.getElementById("game");
    var gl = canvas.getContext('webgl');

    if(!gl) {
        console.log("Webgl not supported, falling back to experimental-version");
        gl = canvas.getContext('experimental-webgl');
    }

    if(!gl) {
        alert("Webgl not supported by your browser");
    }

    gl.clearColor(0.75, 0.85, 0.8, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    var vertextShader = gl.createShader(gl.VERTEX_SHADER);
    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
    
    gl.shaderSource(vertextShader, vertextShaderText);
    gl.shaderSource(fragmentShader, fragmentShaderText);

    gl.compileShader(vertextShader);
    if(!gl.getShaderParameter(vertextShader, gl.COMPILE_STATUS)) {
        console.error("ERROR compiling vertex shader! " + gl.getShaderInfoLog(vertextShader));
        return;
    }

    gl.compileShader(fragmentShader); 
    if(!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        console.error("ERROR compiling vertex shader! " + gl.getShaderInfoLog(fragmentShader));
        return;
    }

    var program = gl.createProgram();
    gl.attachShader(program, vertextShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);
    if(!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        console.error("ERROR compiling program! " + gl.getProgramInfoLog(program));
        return;
    }

    gl.validateProgram(program); 
    if(!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
        console.error("ERROR compiling program! " + gl.getProgramInfoLog(program));
        return;
    }

   

    var triangleVertices = genTriangleFan();


    var triangleVertextBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertextBufferObject);
    // Applies to last bound buffer (triangleVertextBufferObject)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVertices), gl.STATIC_DRAW);

    var positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');
    //var colorAttribLocation = gl.getAttribLocation(program, 'vertColor');

    // Position
    gl.vertexAttribPointer(
        positionAttribLocation, // Attribute Location
        2, // Number of elements per attribute 
        gl.FLOAT, // Type of elements
        gl.FALSE, // Data is normilized
        2 * Float32Array.BYTES_PER_ELEMENT, // Size of individual vertex
        0 // Offset from the beginning of a single vertex to this attribute
    );
    // Color
    /*
    gl.vertexAttribPointer(
        colorAttribLocation, // Attribute Location
        3, // Number of elements per attribute 
        gl.FLOAT, // Type of elements
        gl.FALSE, // Data is normilized
        2 * Float32Array.BYTES_PER_ELEMENT, // Size of individual vertex
        2 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
    );
    */

    gl.enableVertexAttribArray(positionAttribLocation);
    //gl.enableVertexAttribArray(colorAttribLocation);
    gl.useProgram(program);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, triangleVertices.length/2)
};