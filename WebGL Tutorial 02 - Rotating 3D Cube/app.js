var vertextShaderText = 
`
precision mediump float;

attribute vec3 vertPosition;
attribute vec3 vertColor;

uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProjection;

varying vec3 fragColor;

void main()
{
    fragColor = vertColor;
    gl_Position =  mProjection * mView * mWorld * vec4(vertPosition, 1.0);
}
`;

var fragmentShaderText = 
`
precision mediump float;

varying vec3 fragColor;

void main()
{
    gl_FragColor = vec4(fragColor, 1.0);
}
`;

var triangle = {
    vertices: [
        // Front
        0.0, 1.0, 0.0,      1.0, 0.0, 0.0,
        -1.0, -1.0, 1.0,    1.0, 0.0, 0.0,
        1.0, -1.0, 1.0,     1.0, 0.0, 0.0,

        // Right
        0.0, 1.0, 0.0,      0.0, 1.0, 0.0,
        1.0, -1.0, 1.0,     0.0, 1.0, 0.0,
        1.0, -1.0, -1.0,    0.0, 1.0, 0.0,

        // Back
        0.0, 1.0, 0.0,      0.0, 0.0, 0.0,
        -1.0, -1.0, -1.0,   0.0, 0.0, 0.0,
        1.0, -1.0, -1.0,    0.0, 0.0, 0.0,

        // Left
        0.0, 1.0, 0.0,      0.0, 0.0, 1.0,
        -1.0, -1.0, 1.0,    0.0, 0.0, 1.0,
        -1.0, -1.0, -1.0,   0.0, 0.0, 1.0,
        
        // Bottom
        -1.0, -1.0, 1.0,    1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,     1.0, 1.0, 1.0,
        1.0, -1.0, -1.0,    1.0, 1.0, 1.0,
        -1.0, -1.0, -1.0,   1.0, 1.0, 1.0,
    ],
    indices: [
        // Front
        0, 1, 2,

        // Right
        3, 4, 5,

        // Back
        6, 7, 8,

        // Left
        9, 10, 11,

        // Bottom
        12, 13, 14,
        12, 14, 15
    ]
}

var cube = {
    vertices: [
        // Top
		-1.0, 1.0, -1.0,
		-1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		1.0, 1.0, -1.0,

		// Left
		-1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0,
		-1.0, -1.0, -1.0,
		-1.0, 1.0, -1.0,

		// Right
		1.0, 1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,
		1.0, 1.0, -1.0,

		// Front
		1.0, 1.0, 1.0,
		1.0, -1.0, 1.0,
		-1.0, -1.0, 1.0,
		-1.0, 1.0, 1.0,

		// Back
		1.0, 1.0, -1.0,
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, 1.0, -1.0,

		// Bottom
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,
    ],
    colors : [
        // Top
		0.5, 0.5, 0.5,
		0.5, 0.5, 0.5,
		0.5, 0.5, 0.5,
		0.5, 0.5, 0.5,

		// Left
		0.75, 0.25, 0.5,
		0.75, 0.25, 0.5,
		0.75, 0.25, 0.5,
		0.75, 0.25, 0.5,

		// Right
		0.25, 0.25, 0.75,
		0.25, 0.25, 0.75,
		0.25, 0.25, 0.75,
		0.25, 0.25, 0.75,

		// Front
		1.0, 0.0, 0.15,
		1.0, 0.0, 0.15,
		1.0, 0.0, 0.15,
		1.0, 0.0, 0.15,

		// Back
		0.0, 1.0, 0.15,
		0.0, 1.0, 0.15,
		0.0, 1.0, 0.15,
		0.0, 1.0, 0.15,

		// Bottom
		0.5, 0.5, 1.0,
		0.5, 0.5, 1.0,
		0.5, 0.5, 1.0,
		0.5, 0.5, 1.0,
    ],
    indices: [
        // Top
        0, 1, 2,
        0, 2, 3,

        // Left
        5, 4, 6,
        6, 4, 7,

        // Right
        8, 9, 10,
        8, 10, 11,

        // Front
        13, 12, 14,
        15, 14, 12,

        // Back
        16, 17, 18,
        16, 18, 19,

        // Bottom
        21, 20, 22,
        22, 20, 23
    ]
}

var InitDemo = function() {
    console.log("Hello");

    var canvas = document.getElementById("game");
    var gl = canvas.getContext('webgl');

    if(!gl) {
        console.log("Webgl not supported, falling back to experimental-version");
        gl = canvas.getContext('experimental-webgl');
    }

    if(!gl) {
        alert("Webgl not supported by your browser");
    }

    gl.clearColor(0.75, 0.85, 0.8, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.frontFace(gl.CCW)
    gl.cullFace(gl.BACK);

    var vertexShader = compileShader(gl, vertextShaderText, gl.VERTEX_SHADER);
    var fragmentShader = compileShader(gl, fragmentShaderText, gl.FRAGMENT_SHADER);

    var program = createProgram(gl, vertexShader, fragmentShader);

    // Vertex
    var boxVertextBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, boxVertextBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube.vertices), gl.STATIC_DRAW);
    
    // Indices
    var boxIndexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, boxIndexBufferObject);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cube.indices), gl.STATIC_DRAW);

    var positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');
    var colorAttribLocation = gl.getAttribLocation(program, 'vertColor');

    // Position
    gl.vertexAttribPointer(
        positionAttribLocation, // Attribute Location
        3, // Number of elements per attribute 
        gl.FLOAT, // Type of elements
        gl.FALSE, // Data is normilized
        3 * Float32Array.BYTES_PER_ELEMENT, // Size of individual vertex
        0 // Offset from the beginning of a single vertex to this attribute
    );

    // Colors
    var boxColorBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, boxColorBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube.colors), gl.STATIC_DRAW); 

    // Color
    gl.vertexAttribPointer(
        colorAttribLocation, // Attribute Location
        3, // Number of elements per attribute 
        gl.FLOAT, // Type of elements
        gl.FALSE, // Data is normilized
        3 * Float32Array.BYTES_PER_ELEMENT, // Size of individual vertex
        0 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
    );

    gl.enableVertexAttribArray(positionAttribLocation);
    gl.enableVertexAttribArray(colorAttribLocation);

    gl.useProgram(program);

    // Get uniform locations
    var matWorldUniformLocation = gl.getUniformLocation(program, "mWorld");
    var matViewUniformLocation = gl.getUniformLocation(program, "mView");
    var matProjectionUniformLocation = gl.getUniformLocation(program, "mProjection");

    var worldMatrix = new Float32Array(16);
    var viewMatrix = new Float32Array(16);
    var projectiondMatrix = new Float32Array(16);

    glMatrix.mat4.identity(worldMatrix);
    //                              Distance  Look At  What is up dir?
    glMatrix.mat4.lookAt(viewMatrix, [0,0,-8],[0,0,0 ],[0,1,0]);
    glMatrix.mat4.perspective(projectiondMatrix, glMatrix.glMatrix.toRadian(45), canvas.clientWidth / canvas.clientHeight, 0.1, 1000.0);

    gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);
    gl.uniformMatrix4fv(matViewUniformLocation, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(matProjectionUniformLocation, gl.FALSE, projectiondMatrix);


    var xRotationMatrix = new Float32Array(16);
	var yRotationMatrix = new Float32Array(16);

    var identityMatrix = new Float32Array(16);
    glMatrix.mat4.identity(identityMatrix);
    var angle = 0;

    // https://glmatrix.net/docs/module-mat4.html#.translate
    glMatrix.mat4.translate(identityMatrix, identityMatrix, [0, 0, 3]);

    var loop = function() {
        gl.clearColor(0.75, 0.85, 0.8, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        // performance.now = number of milisecionds since window startet
        // diveded by 1000 eq. seconds
        // one full rotation every 6 seconds                             
        angle = performance.now() / 1000 / 2 * 2 * Math.PI;
        
        // https://glmatrix.net/docs/module-mat4.html#.rotate
        glMatrix.mat4.identity(worldMatrix);
        glMatrix.mat4.rotate(worldMatrix, worldMatrix, angle,[0, 1, 0]);
		glMatrix.mat4.rotate(worldMatrix, worldMatrix, angle / 4, [1, 0, 0]);
        glMatrix.mat4.translate(worldMatrix, worldMatrix, [-1, 0, 3]);
		//glMatrix.mat4.mul(worldMatrix, yRotationMatrix, xRotationMatrix);
        gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);

        gl.drawElements(gl.TRIANGLES, cube.indices.length, gl.UNSIGNED_SHORT, 0);


        angle = performance.now() / 1000 / 6 * 2 * Math.PI;

        glMatrix.mat4.identity(worldMatrix);
        glMatrix.mat4.rotate(yRotationMatrix, identityMatrix, angle,[1, 1, 0]);
		glMatrix.mat4.rotate(xRotationMatrix, identityMatrix, angle / 2, [1, 0, 0]);
		glMatrix.mat4.mul(worldMatrix, yRotationMatrix, xRotationMatrix);
        gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);

        gl.drawElements(gl.TRIANGLES, cube.indices.length, gl.UNSIGNED_SHORT, 0);


        angle = performance.now() / 1000 / 4 * 2 * Math.PI;

        glMatrix.mat4.identity(worldMatrix);
        glMatrix.mat4.rotate(yRotationMatrix, identityMatrix, angle,[-1, 1, 0]);
		glMatrix.mat4.rotate(xRotationMatrix, identityMatrix, angle / 2, [1, 0, 0]);
		glMatrix.mat4.mul(worldMatrix, yRotationMatrix, xRotationMatrix);
        gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);

        gl.drawElements(gl.TRIANGLES, cube.indices.length, gl.UNSIGNED_SHORT, 0);

        
        requestAnimationFrame(loop);

    }

    requestAnimationFrame(loop);
};

/**
 * Creates and compiles a shader.
 *
 * @param {!WebGLRenderingContext} gl The WebGL Context.
 * @param {string} shaderSource The GLSL source code for the shader.
 * @param {number} shaderType The type of shader, VERTEX_SHADER or
 *     FRAGMENT_SHADER.
 * @return {!WebGLShader} The shader.
 */
 function compileShader(gl, shaderSource, shaderType) {
    // Create the shader object
    var shader = gl.createShader(shaderType);
   
    // Set the shader source code.
    gl.shaderSource(shader, shaderSource);
   
    // Compile the shader
    gl.compileShader(shader);
   
    // Check if it compiled
    var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!success) {
      // Something went wrong during compilation; get the error
      throw "could not compile shader:" + gl.getShaderInfoLog(shader);
    }
   
    return shader;
  }

  /**
 * Creates a program from 2 shaders.
 *
 * @param {!WebGLRenderingContext) gl The WebGL context.
 * @param {!WebGLShader} vertexShader A vertex shader.
    * @param {!WebGLShader} fragmentShader A fragment shader.
    * @return {!WebGLProgram} A program.
    */
  function createProgram(gl, vertexShader, fragmentShader) {
    // create a program.
    var program = gl.createProgram();
   
    // attach the shaders.
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
   
    // link the program.
    gl.linkProgram(program);
   
    // Check if it linked.
    var success = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!success) {
        // something went wrong with the link
        throw ("program filed to link:" + gl.getProgramInfoLog (program));
    }

    gl.validateProgram(program); 
    if(!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
        console.error("ERROR compiling program! " + gl.getProgramInfoLog(program));
        return;
    }
   
    return program;
  };