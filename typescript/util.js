


/**
 * Requires vertex shader to have
 * 
 * uniform mat4 mWorld;
 * 
 * uniform mat4 mView;
 * 
 * uniform mat4 mProjection;
 * 
 * gl_Position =  mProjection * mView * mWorld * vec4(vertPosition, 1.0);
 * @param {!WebGLRenderingContext} gl The WebGL Context. 
 * @param {!WebGLProgram} program The program. 
 * @param {canvas} canvas 
 * @param {number[]} distance The distance to lookAt
 * @param {number[]} lookAt The position to look at
 * @param {number[]} upDir 
 */
const cameraSettings = (gl, program, canvas, distance = [0, 0, -3], lookAt = [0, 0, 0], upDir = [0, 1, 0]) => {
    // Get uniform locations
    let matWorldUniformLocation = gl.getUniformLocation(program, "mWorld");
    let matViewUniformLocation = gl.getUniformLocation(program, "mView");
    let matProjectionUniformLocation = gl.getUniformLocation(program, "mProjection");

    let worldMatrix = new Float32Array(16);
    let viewMatrix = new Float32Array(16);
    let projectionMatrix = new Float32Array(16);

    glMatrix.mat4.identity(worldMatrix);
    glMatrix.mat4.lookAt(viewMatrix, distance, lookAt, upDir);
    glMatrix.mat4.perspective(projectionMatrix, glMatrix.glMatrix.toRadian(45), canvas.clientWidth / canvas.clientHeight, 0.1, 1000.0);

    gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);
    gl.uniformMatrix4fv(matViewUniformLocation, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(matProjectionUniformLocation, gl.FALSE, projectionMatrix);

    return {
        matrix: {
            world: worldMatrix,
            view: viewMatrix,
            projection: projectionMatrix
        },
        location: {
            world: matWorldUniformLocation,
            view: matViewUniformLocation,
            projection: matProjectionUniformLocation
        }

    }
}

/**
* 
* @param {!WebGLRenderingContext} gl The WebGL Context.
* @param {object} obj Object to draw 
* @returns 
*/
const createBuffer = (gl, obj) => {
    // Vertex
    let vertextBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertextBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(obj.vertices), gl.STATIC_DRAW);
    //gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // Indices
    let indexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBufferObject);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(obj.indices), gl.STATIC_DRAW);
    //gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    console.log("Buffer created");

    return {
        vbo: vertextBufferObject,
        ibo: indexBufferObject
    }
}

/**
* 
* @param {!WebGLRenderingContext} gl The WebGL Context.
* @param {!WebGLProgram} A program.
* @param {*} buffer 
* @param {object} obj Object to draw 
* @param {number} obj.vertices
* @param {number} obj.indices
* @param {object} pointer Containing settings vor attrib pointer 
* @param {number} pointer.num
* @param {number} pointer.size
* @param {number} pointer.offset
* @param {*} drawMode gl draw mode 
*/
const drawBuffer = (gl, program, buffer, obj, pointer = { num: 2, size: 2, offset: 0 }, drawMode = gl.TRIANGLES) => {
    gl.useProgram(program);
    let positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');

    // VBO
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer.vbo);
    gl.vertexAttribPointer(
        positionAttribLocation, // Attribute Location
        pointer.num, // Number of elements per attribute 
        gl.FLOAT, // Type of elements
        gl.FALSE, // Data is normilized
        pointer.size * Float32Array.BYTES_PER_ELEMENT, // Size of individual vertex
        pointer.offset // Offset from the beginning of a single vertex to this attribute
    );
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.enableVertexAttribArray(positionAttribLocation);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer.ibo);
    gl.drawElements(drawMode, obj.indices.length, gl.UNSIGNED_SHORT, 0);

}

/**
* Creates and compiles a shader.
*
* @param {!WebGLRenderingContext} gl The WebGL Context.
* @param {string} shaderSource The GLSL source code for the shader.
* @param {number} shaderType The type of shader, VERTEX_SHADER or
*     FRAGMENT_SHADER.
* @return {!WebGLShader} The shader.
*/
const compileShader = (gl, shaderSource, shaderType) => {
    // Create the shader object
    let shader = gl.createShader(shaderType);

    // Set the shader source code.
    gl.shaderSource(shader, shaderSource);

    // Compile the shader
    gl.compileShader(shader);

    // Check if it compiled
    let success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!success) {
        // Something went wrong during compilation; get the error
        throw "could not compile shader:" + gl.getShaderInfoLog(shader);
    }

    console.log("Shader compiled");

    return shader;
}

/**
* Creates a program from 2 shaders.
*
* @param {!WebGLRenderingContext) gl The WebGL context.
* @param {!WebGLShader} vertexShader A vertex shader.
  * @param {!WebGLShader} fragmentShader A fragment shader.
  * @return {!WebGLProgram} A program.
  */
const createProgram = (gl, vertexShader, fragmentShader) => {
    // create a program.
    let program = gl.createProgram();

    // attach the shaders.
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);

    // link the program.
    gl.linkProgram(program);

    // Check if it linked.
    let success = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!success) {
        // something went wrong with the link
        throw ("program filed to link:" + gl.getProgramInfoLog(program));
    }

    gl.validateProgram(program);
    if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
        console.error("ERROR compiling program! " + gl.getProgramInfoLog(program));
        return;
    }

    console.log("Program created");

    return program;
};

const loadSrc = (url) => {
    var req = new XMLHttpRequest();
    req.open("GET", url, false);
    req.send(null);
    return (req.status == 200) ? req.responseText : null;
}

// This is just a simple demonstration. Wavefront OBJ is not fully supported!
// See https://en.wikipedia.org/wiki/Wavefront_.obj_file for more information.
const fetchModel = async (location) => {
	
	// fetch is explained at https://www.youtube.com/watch?v=tc8DU14qX6I.
	let response = await fetch(location);
	let txt = await response.text();
	let lines = txt.split(/\r*\n/);
	
	let v = [];
	let vt = [];
	let vn = [];
	let buffer = [];

	for (let line of lines) {
		let data = line.trim().split(/\s+/);
		let type = data.shift();
		if (type == 'v') {
			v.push(data.map(parseFloat));
		}
		else if (type == 'vt') {
			vt.push(data.map(parseFloat));
		}
		else if (type == 'vn') {
			vn.push(data.map(parseFloat));
		}
		else if (type == 'f') {
			for (let fp of data) {
				let idx = fp.split('/').map( (x) => {return parseInt(x)} );
				v[idx[0]-1].forEach( (x) => {buffer.push(x)} );
				vt[idx[1]-1].forEach( (x) => {buffer.push(x)} );
				vn[idx[2]-1].forEach( (x) => {buffer.push(x)} );
			}
		}
	}
	return buffer;
};

export {cameraSettings, createBuffer, drawBuffer, compileShader, createProgram, loadSrc, fetchModel}