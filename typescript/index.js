import * as util from './util.js';

const rectangle = {
    vertices: [
        -1, 1,
        -1, -1,
        1, -1,
        1, 1
    ],
    indices: [
        0, 1, 3,
        3, 1, 2
    ],
    colors: [
        0.2, 0.5, 0.5,
        0.1, 0.7, 0.1,
        0.75, 0.2, 0.75,
        0.25, 0.25, 0.5,
    ]
}

const init = () => {
    console.log("Running...")

    // Get shader code from file
    const vertCode = util.loadSrc('shaders/vs.glsl')
    const fragCode = util.loadSrc('shaders/fs.glsl')

    const canvas = document.getElementById("game")
    const gl = canvas.getContext('webgl')

    if (!gl) {
        console.log("Webgl not supported, falling back to experimental-version")
        gl = canvas.getContext('experimental-webgl')
    }

    if (!gl) {
        alert("Webgl not supported by your browser")
    }

    // Compile Shaders
    const vertexShader = util.compileShader(gl, vertCode, gl.VERTEX_SHADER)
    const fragmentShader = util.compileShader(gl, fragCode, gl.FRAGMENT_SHADER)

    // Create Program
    const program = util.createProgram(gl, vertexShader, fragmentShader)
    const buffer = util.createBuffer(gl, rectangle)

    // Colors
    const boxColorBufferObject = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, boxColorBufferObject)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(rectangle.colors), gl.STATIC_DRAW)

    const colorAttribLocation = gl.getAttribLocation(program, 'vertColor')

    // Color
    gl.vertexAttribPointer(
        colorAttribLocation, // Attribute Location
        3, // Number of elements per attribute 
        gl.FLOAT, // Type of elements
        gl.FALSE, // Data is normilized
        3 * Float32Array.BYTES_PER_ELEMENT, // Size of individual vertex
        0 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
    );

    gl.enableVertexAttribArray(colorAttribLocation);
    gl.useProgram(program);

    const camera = util.cameraSettings(gl, program, canvas, [0, 0, 10]);

    let loop = () => {
        gl.clearColor(0.4, 0.4, 0.4, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        util.drawBuffer(gl, program, buffer, rectangle, { num: 2, size: 2, offset: 0 }, gl.TRIANGLES);

        requestAnimationFrame(loop);
    }

    requestAnimationFrame(loop);
}

window.init = init;