import * as util from '../util.js';

var vertextShaderText =
    `
precision mediump float;

attribute vec3 vertPosition;
attribute vec3 vertColor;

uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProjection;

varying vec3 fragColor;

void main()
{
    fragColor = vertColor;
    gl_Position =  mProjection * mView * mWorld * vec4(vertPosition, 1.0);
}
`;

var fragmentShaderText =
    `
precision mediump float;

varying vec3 fragColor;

void main()
{
    gl_FragColor = vec4(fragColor, 1.0);
}
`;

let cube = {
    vertices: [
        // Top
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,

        // Left
        -1.0, 1.0, 1.0,
        -1.0, -1.0, 1.0,
        -1.0, -1.0, -1.0,
        -1.0, 1.0, -1.0,

        // Right
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
        1.0, 1.0, -1.0,

        // Front
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        -1.0, -1.0, 1.0,
        -1.0, 1.0, 1.0,

        // Back
        1.0, 1.0, -1.0,
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, 1.0, -1.0,

        // Bottom
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,
    ],
    colors: [
        // Top
        0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,
        0.5, 0.5, 0.5,

        // Left
        0.75, 0.25, 0.5,
        0.75, 0.25, 0.5,
        0.75, 0.25, 0.5,
        0.75, 0.25, 0.5,

        // Right
        0.25, 0.25, 0.75,
        0.25, 0.25, 0.75,
        0.25, 0.25, 0.75,
        0.25, 0.25, 0.75,

        // Front
        1.0, 0.0, 0.15,
        1.0, 0.0, 0.15,
        1.0, 0.0, 0.15,
        1.0, 0.0, 0.15,

        // Back
        0.0, 1.0, 0.15,
        0.0, 1.0, 0.15,
        0.0, 1.0, 0.15,
        0.0, 1.0, 0.15,

        // Bottom
        0.5, 0.5, 1.0,
        0.5, 0.5, 1.0,
        0.5, 0.5, 1.0,
        0.5, 0.5, 1.0,
    ],
    indices: [
        // Top
        0, 1, 2,
        0, 2, 3,

        // Left
        5, 4, 6,
        6, 4, 7,

        // Right
        8, 9, 10,
        8, 10, 11,

        // Front
        13, 12, 14,
        15, 14, 12,

        // Back
        16, 17, 18,
        16, 18, 19,

        // Bottom
        21, 20, 22,
        22, 20, 23
    ]
}

export function init() {
    console.log("Running...");

    var canvas = document.getElementById("game");
    var gl = canvas.getContext('webgl');

    if (!gl) {
        console.log("Webgl not supported, falling back to experimental-version");
        gl = canvas.getContext('experimental-webgl');
    }

    if (!gl) {
        alert("Webgl not supported by your browser");
    }

    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.frontFace(gl.CCW)
    gl.cullFace(gl.BACK);

    // Compile Shaders
    let vertexShader = util.compileShader(gl, vertextShaderText, gl.VERTEX_SHADER);
    let fragmentShader = util.compileShader(gl, fragmentShaderText, gl.FRAGMENT_SHADER);

    // Create Program
    let program = util.createProgram(gl, vertexShader, fragmentShader);
    let buffer = util.createBuffer(gl, cube);

    // Colors
    var boxColorBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, boxColorBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cube.colors), gl.STATIC_DRAW);

    var colorAttribLocation = gl.getAttribLocation(program, 'vertColor');

    // Color
    gl.vertexAttribPointer(
        colorAttribLocation, // Attribute Location
        3, // Number of elements per attribute 
        gl.FLOAT, // Type of elements
        gl.FALSE, // Data is normilized
        3 * Float32Array.BYTES_PER_ELEMENT, // Size of individual vertex
        0 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
    );

    gl.enableVertexAttribArray(colorAttribLocation);
    gl.useProgram(program);

    let camera = util.cameraSettings(gl, program, canvas, [-3, 3, -10]);

    let loop = () => {
        gl.clearColor(0.4, 0.4, 0.4, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


        requestAnimationFrame(loop);
    }

    requestAnimationFrame(loop);
}

/**
 * CULLING AUS
 * 
 */

/**
 * Creates vertecies for circle
 * @param {number} steps 
 * @param {number} radius 
 * @returns 
 */
 function generateCircle(steps = 15, radius = 1) {

    let cords = [];

    cords.push(0);
    cords.push(0);

    for(let i = 0; i < 2*Math.PI+0.001; i+=(2*Math.PI/steps) ){

        let tmp = radius * Math.cos(i);
        let tmp2 = radius * Math.sin(i);

        cords.push(tmp);
        cords.push(tmp2);

    }

    return cords;

}

window.init = init;