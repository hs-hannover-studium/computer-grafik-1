import * as util from '../util.js';

var vertextShaderText = 
`
precision mediump float;

attribute vec2 vertPosition;

void main()
{
    gl_Position = vec4(vertPosition, 0.0, 1.0);
}
`;

let fragmentShaderText = 
`
precision mediump float;

void main()
{
    gl_FragColor = vec4(1.0, 0.0, 0.0 , 1.0);
}
`;

export function init() {
    console.log("Running...");

    var canvas = document.getElementById("game");
    var gl = canvas.getContext('webgl');

    if(!gl) {
        console.log("Webgl not supported, falling back to experimental-version");
        gl = canvas.getContext('experimental-webgl');
    }

    if(!gl) {
        alert("Webgl not supported by your browser");
    }

    gl.clearColor(0.75, 0.85, 0.8, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Compile Shaders
    let vertexShader = util.compileShader(gl, vertextShaderText, gl.VERTEX_SHADER);
    let fragmentShader = util.compileShader(gl, fragmentShaderText, gl.FRAGMENT_SHADER);

    // Create Program
    let program = util.createProgram(gl, vertexShader, fragmentShader);
    let buffer = gl.createBuffer();

    let vertices = generateCircle(45, 0.5);

    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

    let positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');
    gl.vertexAttribPointer(
        positionAttribLocation, // Attribute Location
        2, // Number of elements per attribute 
        gl.FLOAT, // Type of elements
        gl.FALSE, // Data is normilized
        2 * Float32Array.BYTES_PER_ELEMENT, // Size of individual vertex
        0 // Offset from the beginning of a single vertex to this attribute
    );

    gl.enableVertexAttribArray(positionAttribLocation);
    gl.useProgram(program);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, vertices.length/2);
}

window.init = init;