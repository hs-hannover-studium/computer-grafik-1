import * as util from '../util.js';

var vertextShaderText = 
`
precision mediump float;

uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProjection;

attribute vec2 vertPosition;

void main()
{
    //gl_Position = vec4(vertPosition, 0.0, 1.0);
    gl_Position =  mProjection * mView * mWorld * vec4(vertPosition, 0.0, 1.0);
}
`;

let fragmentShaderText = 
`
precision mediump float;

void main()
{
    gl_FragColor = vec4(1.0, 0.0, 0.0 , 1.0);
}
`;

let obj = {
    vertices: [
        -1.0, 1.0,
        -1.0, -0.7,
        -0.9, -0.7,
        -0.9, -0.05,
        0.9, -0.05,
        0.9, -0.7,
        1.0, -0.7,
        1.0, 1.0,
        0.9, 1.0,
        0.9, 0.05,
        -0.9, 0.05,
        -0.9, 1.0,

        -1.0, -0.9,
        -1.0, -1.0,
        1.0, -1.0,
        1.0, -0.9
    ],
    indices: [
        0, 1, 11,
        1, 2, 11,

        // Upper invisible box
        11, 11, 8,
        8, 8, 9,

        9, 10, 3,
        9, 3 , 4,

        // Lower invisible box
        // 4, 3, 2
        // 4, 2, 5
        4, 4, 2,    
        2, 2, 5,    

        5, 6, 7,
        7, 8, 5,

        // invisible box
        // 6, 1, 12,
        // 6, 12, 15,
        6, 6, 12, 
        12, 12, 15,

        15, 12, 13,
        13, 14, 15        
    ]
}

export function init() {
    console.log("Running...");

    var canvas = document.getElementById("game");
    var gl = canvas.getContext('webgl');

    if(!gl) {
        console.log("Webgl not supported, falling back to experimental-version");
        gl = canvas.getContext('experimental-webgl');
    }

    if(!gl) {
        alert("Webgl not supported by your browser");
    }

    gl.clearColor(0.75, 0.85, 0.8, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Compile Shaders
    let vertexShader = util.compileShader(gl, vertextShaderText, gl.VERTEX_SHADER);
    let fragmentShader = util.compileShader(gl, fragmentShaderText, gl.FRAGMENT_SHADER);

    // Create Program
    let program = util.createProgram(gl, vertexShader, fragmentShader);
    let buffer = util.createBuffer(gl, obj);

    gl.useProgram(program);

    let camera = util.cameraSettings(gl, program, canvas, [0,0,-3])

    // World matrix manipulation
    glMatrix.mat4.identity(camera.matrix.world);
    glMatrix.mat4.scale(camera.matrix.world, camera.matrix.world, [0.5, 1, 0]);

    gl.uniformMatrix4fv(camera.location.world, gl.FALSE, camera.matrix.world);
    
    // END

    util.drawBuffer(gl, program, buffer, obj, {num: 2, size: 2, offset: 0}, gl.TRIANGLE_STRIP);
}

window.init = init;