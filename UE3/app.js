import * as util from '../util.js';

let vertextShaderText = 
`
precision mediump float;

attribute vec3 vertPosition;
attribute float vertValue;

uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProjection;

varying float fragValue;

void main()
{
    fragValue = vertValue;
    gl_Position =  mProjection * mView * mWorld * vec4(vertPosition, 1.0);
}
`;

let fragmentShaderText = 
`
precision mediump float;

uniform vec3 fragColor;
uniform vec3 fragColor2;
varying float fragValue;

void main()
{
    vec3 color = mix(fragColor, fragColor2, fragValue);
    //vec3 color = mix(fragColor, fragColor2, step(0.5, fragValue));
    //vec3 color = mix(fragColor, fragColor2, smoothstep(0.4, 0.6, fragValue));
    gl_FragColor.rgb = color;
    gl_FragColor.a = 1.0;
}
`;

let rectangle = {
    vertices: [
        // Front
		-1.0, 0.1,   0.0, // Right Top
		1.0, 0.1,    1.0, // Top Left
		1.0, -0.1,   1.0, // Bottom Left
		-1.0, -0.1,  0.0, // Right Bottom
    ],
    indices: [
        // Front
        0, 1, 2,
        0, 2, 3
    ]
}
    
export function init() {
    let canvas = document.getElementById("game");
    let gl = canvas.getContext('webgl');

    if(!gl) {
        console.log("Webgl not supported, falling back to experimental-version");
        gl = canvas.getContext('experimental-webgl');
    }

    if(!gl) {
        alert("Webgl not supported by your browser");
    }

    // WebGL Settings
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.frontFace(gl.CCW)
    gl.cullFace(gl.BACK);

    // Compile Shadres
    let vertexShader = util.compileShader(gl, vertextShaderText, gl.VERTEX_SHADER);
    let fragmentShader = util.compileShader(gl, fragmentShaderText, gl.FRAGMENT_SHADER);

    let program = util.createProgram(gl, vertexShader, fragmentShader);

    let buffer = util.createBuffer(gl, rectangle);
    
    let valueAttribLocation = gl.getAttribLocation(program, 'vertValue');
    gl.vertexAttribPointer(
        valueAttribLocation,
        1,
        gl.FLOAT,
        gl.FALSE,
        3 * Float32Array.BYTES_PER_ELEMENT,
        2 * Float32Array.BYTES_PER_ELEMENT
    )

    gl.enableVertexAttribArray(valueAttribLocation);

    gl.useProgram(program);

    // Set fragColor values
    let colorUniformLocation = gl.getUniformLocation(program, 'fragColor');
    gl.uniform3fv(colorUniformLocation, [0.0, 1.0, 0.0]);
    let color2UniformLocation = gl.getUniformLocation(program, 'fragColor2');
    gl.uniform3fv(color2UniformLocation, [0.0, 0.0, 1.0]);

    util.cameraSettings(gl, program, canvas);

    gl.clearColor(0.75, 0.85, 0.8, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    util.drawBuffer(gl, program, buffer, rectangle, {num: 2, size: 3, offset: 0});
};

window.init = init;